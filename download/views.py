from django.shortcuts import render
from django.http import HttpResponse
from pytube import YouTube

# Create your views here.

def home(request):
    return HttpResponse("wanna play?")

def index(request):
    return render(request, 'index.html')

def get_youtube_data(request):
    if request.method == 'GET':
        link = request.GET['link']
        youtube = YouTube(link)
        if youtube:
            resolution = youtube.get_video
            if resolution:
                return HttpResponse(resolution)
            else:
                return HttpResponse("NTHING")
        else:
            return  HttpResponse("NTHING")
    else:
        return HttpResponse('ERR')